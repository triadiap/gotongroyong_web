<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Bantuan extends CI_Controller{
    function index(){
        $data["data"] = $this->db->query("select * from m_jenis_bantuan");
        $this->load->view("master/bantuan",$data);
    }
    function add(){
        $data["bantuan"] = $this->input->post("bantuan");
        $data["keterangan"] = $this->input->post("keterangan");
        $id = $this->input->post("id");
        if($id == "0"){
            $this->db->insert("m_jenis_bantuan",$data);
        }else{
            $this->db->where("id",$id);
            $this->db->update("m_jenis_bantuan",$data);
        }
    }
    function delete(){
        $this->db->where("id",$this->input->post("id"));
        $this->db->delete("m_jenis_bantuan");        
    }
    function detail(){        
        $this->db->where("id",$this->input->post("id"));
        $data = $this->db->get("m_jenis_bantuan")->row();
        echo json_encode($data);
    }
}