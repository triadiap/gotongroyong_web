<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Laporan extends CI_Controller{
    function index(){
        $data["data"] = $this->db->query("select t_foto.*, m_users.nama from t_foto inner join m_users on m_users.id = t_foto.id_user order by created_date desc");
        $this->load->view("bantuan/laporan",$data);
    }
    function delete(){
        $this->db->where("id",$this->input->post("id"));
        $this->db->delete("t_foto");        
    }
    function detail(){        
        $id = $this->input->post("id");
        $data = $this->db->query("select t_foto.*, m_users.nama from t_foto inner join m_users on m_users.id = t_foto.id_user where  t_foto.id = '$id'")->row();
        echo json_encode($data);
    }
}