<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Permintaan extends CI_Controller{
    function index(){
        $data["data"] = $this->db->query("select t_request.id,provinsi.provinsi,kabupaten.kabupaten,kelurahan.kelurahan,kecamatan.kecamatan,nama,m_jenis_bantuan.bantuan,`status` from t_request 
        inner join provinsi on provinsi.kode = t_request.provinsi
        inner join kabupaten on kabupaten.kode = t_request.kabupaten
        inner join kelurahan on kelurahan.kode = t_request.kelurahan
        inner join kecamatan on kecamatan.kode = t_request.kecamatan
        inner join m_jenis_bantuan on m_jenis_bantuan.id = t_request.jenis_bantuan");
        $this->load->view("bantuan/permintaan",$data);
    }
    function add(){
        $data["status"] = $this->input->post("status");
        $data["keterangan"] = $this->input->post("keterangan");
        $id = $this->input->post("id");
        $this->db->where("id",$id);
        $this->db->update("t_request",$data);
    }
    function delete(){
        $this->db->where("id",$this->input->post("id"));
        $this->db->delete("t_request");        
    }
    function detail(){        
        $id = $this->input->post("id");
        $data = $this->db->query("select t_request.id,provinsi.provinsi,kabupaten.kabupaten,kelurahan.kelurahan,kecamatan.kecamatan,t_request.nama,m_jenis_bantuan.bantuan,`status`, m_users.nama pic_nama, m_users.no_tlpn pic_tlpn, m_users.pekerjaan pic_pekerjaan,t_request.keterangan from t_request 
        inner join provinsi on provinsi.kode = t_request.provinsi
        inner join kabupaten on kabupaten.kode = t_request.kabupaten
        inner join kelurahan on kelurahan.kode = t_request.kelurahan
        inner join kecamatan on kecamatan.kode = t_request.kecamatan
        inner join m_jenis_bantuan on m_jenis_bantuan.id = t_request.jenis_bantuan
    inner join m_users on m_users.id = t_request.requestor
    where t_request.id = '$id'")->row();
        echo json_encode($data);
    }
}