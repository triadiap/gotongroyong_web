<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Services extends CI_Controller{
  function index(){
    echo "ini api";
  }
  function register(){
	  
    $data["nomor_telpn"] = "0".$this->input->post("nomor_telpon");
    $this->db->where($data);
    $result = $this->db->get("m_anggota")->row();
    if($result){
      echo json_encode($result);
	  die();
    }
    $data["foto_ktp"] = "/foto/".$this->input->post("foto");
    $data["foto"] = "/foto/".$this->input->post("foto");
    $data["nama"] = $this->input->post("nama");
    $data["email"] = $this->input->post("email");
    $data["password"] = md5($this->input->post("password"));
    $data["input_status"] = "0";
    $data["register_date"] = date("d-m-y");
    $this->db->insert("m_anggota",$data);
    $id = $insert_id = $this->db->insert_id();
    $this->db->where("id",$id);
    $result = $this->db->get("m_anggota")->row();
    if($result){
      echo json_encode($result);
    }else{
      $result["id"] = "0";
      echo json_encode($result);
    }
  }
  function mobile_login(){
    $data["nomor_telpn"] = "0".$this->input->post("nomor_telpon");
    $this->db->where($data);
    $result = $this->db->get("m_anggota")->row();
    if($result){
      echo json_encode($result);
    }else{
      $result["id"] = "0";
      echo json_encode($result);
    }
  }
  function upload(){
      $base64Image = $this->input->post("image");
      $filename = $this->input->post("name");
      $decoded=base64_decode($base64Image);
      //file_put_contents($filename,$decoded);
      //echo FCPATH."/foto/".$filename;
      file_put_contents(FCPATH."foto/".$filename,$decoded);
  }
  function login(){
    $data["nomor_telpn"] = $this->input->post("username");
    $data["password"] = md5($this->input->post("password"));
    $this->db->where($data);
    $result = $this->db->get("m_anggota")->row();
    if($result){
      echo json_encode($result);
    }else{
      $result["id"] = "0";      
      echo json_encode($result);
    }
  }
  function getData(){
    $data["id"] = $this->input->get("id");
    $this->db->where($data);
    $result = $this->db->get("m_anggota")->row();
    if($result){
      echo json_encode($result);
    }else{
      $result["id"] = "0";
      echo json_encode($result);
    }
  }
  function getFoto(){    
    $data["id"] = $this->input->get("id");
    $this->db->where($data);
    $result = $this->db->get("m_anggota")->row();
    $filename = FCPATH."foto/user.png";
    if($result){
      if($result->foto!=""){
        $filename=FCPATH.$result->foto; //<-- specify the image  file        
      }
    }
    if(file_exists($filename)){ 
      $mime = mime_content_type($filename); //<-- detect file type
      header('Content-Length: '.filesize($filename)); //<-- sends filesize header
      header("Content-Type: $mime"); //<-- send mime-type header
      header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
      readfile($filename); //<--reads and outputs the file onto the output buffer
      die(); //<--cleanup
      exit; //and exit
    }
  }

  function ttd_ketua(){    
    $filename = FCPATH."foto/ttd_ketua.jpg";
    if(file_exists($filename)){ 
      $mime = mime_content_type($filename); //<-- detect file type
      header('Content-Length: '.filesize($filename)); //<-- sends filesize header
      header("Content-Type: $mime"); //<-- send mime-type header
      header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
      readfile($filename); //<--reads and outputs the file onto the output buffer
      die(); //<--cleanup
      exit; //and exit
    }
  }
  function ttd_sekjen(){    
    $filename = FCPATH."foto/ttd_sekjen.jpg";
    if(file_exists($filename)){ 
      $mime = mime_content_type($filename); //<-- detect file type
      header('Content-Length: '.filesize($filename)); //<-- sends filesize header
      header("Content-Type: $mime"); //<-- send mime-type header
      header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
      readfile($filename); //<--reads and outputs the file onto the output buffer
      die(); //<--cleanup
      exit; //and exit
    }
  }
  
	function update_info(){
		$id = $this->input->post("id");
		if($this->input->post("foto")!=""){
			$data["foto"] = "/foto/".$this->input->post("foto");
		}
		$data["nama"] = $this->input->post("nama");
		$password = $this->input->post("password");
		if($password!=""){
			$data["password"] = md5($password);
		}
		$data["nomor_ktp"] = $this->input->post("nomor_ktp");
		$this->db->where("id",$id);
		$this->db->update("m_anggota",$data);
		$msg["status"] = "1";
		echo json_encode($msg);
		
	}
  
  function getQR(){
    $this->load->library('ciqrcode');
    $params["data"] = "BEGIN:VCARD\nVERSION:2.1\nN:John Doe\nTEL;HOME;VOICE:555-555-5555\nTEL;WORK;VOICE:666-666-6666\nEMAIL:email@example.com\nORG:TEC-IT\nURL:https://www.example.com\nEND:VCARD";
    $id = $this->input->get("id");
    $filename = FCPATH.'tes.png';
    $this->db->where("id",$id);
    $result = $this->db->get("m_anggota");
    foreach($result->result() as $tmp){
      $params["data"] = "BEGIN:VCARD\nVERSION:2.1\nN:".$tmp->nama."\nTEL;HOME;VOICE:".$tmp->nomor_telpn."\nEMAIL:".$tmp->email."\nORG:partaihanura\nEND:VCARD";    
      $filename = FCPATH.'qr/'.$tmp->id.'.png';
    }
    $params['level'] = 'H';
    $params['size'] = 10;
    $params['savename'] = $filename;
    $this->ciqrcode->generate($params);    
    if(file_exists($filename)){ 
      $mime = mime_content_type($filename); //<-- detect file type
      header('Content-Length: '.filesize($filename)); //<-- sends filesize header
      header("Content-Type: $mime"); //<-- send mime-type header
      header('Content-Disposition: inline; filename="'.$filename.'";'); //<-- sends filename header
      readfile($filename); //<--reads and outputs the file onto the output buffer
      die(); //<--cleanup
      exit; //and exit
    }
    
  }
}
