<?php

Class User extends CI_Controller{    
    function index(){
        $data["data"] = $this->db->query("select * from m_users");
        $this->load->view("master/user",$data);
    }
    function add(){
        $data["username"] = $this->input->post("username");
        $password = $this->input->post("password");
        if($password !=""){
            $data["password"] = md5($password);
        }
        $data["no_tlpn"] = $this->input->post("no_tlpn");
        $data["type"] = $this->input->post("type");
        $data["no_ktp"] = $this->input->post("no_ktp");
        $data["jenis_kelamin"] = $this->input->post("jenis_kelamin");
        $data["pekerjaan"] = $this->input->post("pekerjaan");
        $data["tanggal_lahir"] = $this->input->post("tanggal_lahir");
        $data["nama"] = $this->input->post("nama");
        $id = $this->input->post("id");
        if($id =="0"){
            $this->db->insert("m_users",$data);
        }else{
            $this->db->where("id",$id);
            $this->db->update("m_users",$data);
        }
    }
    function delete(){
        $this->db->where("id",$this->input->post("id"));
        $this->db->delete("m_users");        
    }
    function detail(){        
        $this->db->where("id",$this->input->post("id"));
        $data = $this->db->get("m_users")->row();
        echo json_encode($data);
    }
}