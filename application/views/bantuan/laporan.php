<?php
$jenis_laporan["1"] = "Bantuan Baik";
$jenis_laporan["2"] = "Bantuan Tidak Di Terima";
$jenis_laporan["3"] = "Bantuan Buruk";
$jenis_laporan["0"] = "Lainnya";
?>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-primary">
                <div class="card-icon">
                    <i class="material-icons">list</i>
                </div>
                <h4 class="card-title">
                    Daftar Permintaan
                </h4>
            </div>
            <div class="card-body ">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>                    
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Laporan</th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach($data->result() as $tmp) : $i++?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $tmp->nama?></td>
                                        <td><?php echo $tmp->created_date;?></td>
                                        <td><?php echo $jenis_laporan[$tmp->jenis_laporan];?></td>
                                        <td><?php echo $tmp->komentar;?></td>
                                        <td class="text-center tetx-white">                                                                               
                                            <a onclick="view_image('<?php echo $tmp->foto?>')" class="btn btn-primary btn-sm btn-round btn-fab btn-fab-mini"><i class="fa fa-image text-white"></i><a> 
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalView" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Foto KTP</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src"" id="image_view" style="width:100%"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-add-title">Detail Permintaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" id="form-add" onsubmit="doAdd();return false;">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Penerima</label>
                        <input id="input-nama" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Bantuan</label>
                        <input id="input-jenis_bantuan" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Provinsi</label>
                        <input id="input-provinsi" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kabupaten</label>
                        <input id="input-kabupaten" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kecamatan</label>
                        <input id="input-kecamatan" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kelurahan</label>
                        <input id="input-kelurahan" type="text" class="form-control" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">PIC</label>
                        <input id="input-pic_nama" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">PIC Tlpn</label>
                        <input id="input-pic_tlpn" type="text" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Pekerjaan</label>
                        <input id="input-pic_pekerjaan" type="text" class="form-control" >
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Status</label>                
                <select id="input-status" class="form-control select" data-style="btn btn-link"   required>
                    <option value="0">Permintaan</option>
                    <option value="1">Bantuan Di Kirim</option>
                    <option value="2">Bantuan Di Terima</option>
                    <option value="3">Gagal Di Termima</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Komentar</label>                
                <input id="input-keterangan" type="text" class="form-control" >
            </div>
            <input type="submit" id="btn-add-submit" style="display:none">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button"  class="btn btn-primary" onclick="$('#btn-add-submit').click()">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
    var menuName = "permintan";
    var selectedID = "0";
    $(function(){
        $(".table").DataTable();
        $('.select select').css('width', '100%');
        $(".select").select2();
        $('.select2').css('width', '100%');
        $('.select2').css('margin-bottom', '10px');
    });
    function doAdd(){        
        $('#modal-add').modal("hide");
        processStart();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>permintaan/add",
            data:{
                id:selectedID,
                status: $("#input-status").val(),
                keterangan : $("#input-keterangan").val()
            }
        }).done(function (msg) {
            processDone();
            $.notify({
                icon: "add_alert",
                message: "Info, <b>"+menuName+" berhasil di simpan</b>"
            }, {
                type: 'success',
                timer: 4000,
                placement: {
                    from: "top",
                    align: "right"
                }
            });
            setTimeout(() => {                
                goMenu("Permintaan","permintaan");
            }, 1000);
        });
    }
    function confirmDelete(id){
        Swal.fire({
            title: 'Hapus data',
            text: "Apakah anda yakin akan meghapus "+menuName+"?",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus'
        }).then((result) => {
            if (result.value) {
                doDelelete(id)
            }
        });
    }
    function getEdit(id){
        selectedID = id;
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>permintaan/detail",
            data: {
                id: id
            },
            statusCode: {
                404: function () {
                    processDone();
                    showError("404 Page not found");
                },
                500: function () {
                    processDone();
                    showError("500 Please contact IT Support");
                }
            },
        }).done(function (msg) {
            var data = eval("("+msg+")");
            $("#input-penerima").val(data.nama);
            $("#input-jenis_bantuan").val(data.bantuan);
            $("#input-provinsi").val(data.provinsi);
            $("#input-kabupaten").val(data.kabupaten);
            $("#input-kecamatan").val(data.kecamatan);
            $("#input-kelurahan").val(data.kelurahan);
            $("#input-pic_nama").val(data.pic_nama);
            $("#input-pic_tlpn").val(data.pic_tlpn);
            $("#input-pic_pekerjaan").val(data.pic_pekerjaan);
            $("#input-status").val(data.status).trigger("change");
            $("#input-keterangan").val(data.keterangan);
            $('#modal-add').modal("show");
        });
    }
    function doDelelete(id){
        processStart();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>permintaan/delete",
            data: {
                id: id
            },
            statusCode: {
                404: function () {
                    processDone();
                    showError("404 Page not found");
                },
                500: function () {
                    processDone();
                    showError("500 Please contact IT Support");
                }
            },
        }).done(function (msg) {
            processDone();
            if (msg != "") {
                $.notify({
                    icon: "add_alert",
                    message: "Warning, <b>" + msg + "</b>"
                }, {
                    type: 'danger',
                    timer: 4000,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                });
            } else {
                $.notify({
                    icon: "add_alert",
                    message: "Info, <b>"+menuName+" berhasil di hapus</b>"
                }, {
                    type: 'info',
                    timer: 1000,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                });
                goMenu("Permintaan","permintaan");
            }
        });
    }
    function view_image(foto){        
        $("#image_view").attr("src","<?php echo base_url()?>"+foto);
        $('#modalView').modal("show");
    }
</script>