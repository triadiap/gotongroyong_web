<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-primary">
                <div class="card-icon">
                    <i class="material-icons">list</i>
                </div>
                <h4 class="card-title">
                    Daftar Bantuan
                </h4>
            </div>
            <div class="card-body ">
                <div class="row">   
                    <div class="col-md-12">
                        <input type="button" class="btn btn-primary btn-round" value="Add" onclick="add()" />
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>                    
                                <tr>
                                    <th>#</th>
                                    <th>Jenis Bantuan</th>
                                    <th>Keterangan</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach($data->result() as $tmp) : $i++?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $tmp->bantuan?></td>
                                        <td><?php echo $tmp->keterangan;?></td>
                                        <td class="text-center tetx-white">                                    
                                            <a onclick="getEdit('<?php echo $tmp->id?>')" class="btn btn-primary btn-sm btn-round btn-fab btn-fab-mini"><i class="fa fa-pencil-alt text-white"></i><a>
                                            <a onclick="confirmDelete('<?php echo $tmp->id?>')" class="btn btn-danger btn-sm btn-round btn-fab btn-fab-mini"><i class="fa fa-trash text-white"></i><a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-add-title">Tambah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" id="form-add" onsubmit="doAdd();return false;">
            <div class="form-group">
                <label for="exampleInputEmail1">Jenis Bantuan</label>
                <input id="input-bantuan" type="text" class="form-control" placeholder="Jenis Bantuan" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Keterangan</label>
                <textarea id="input-keterangan" class="form-control" placeholder="Keterangan"></textarea>
            </div>
            <input type="submit" id="btn-add-submit" style="display:none">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button"  class="btn btn-primary" onclick="$('#btn-add-submit').click()">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
    var menuName = "jenis bantuan";
    var selectedID = "0";
    $(function(){
        $(".select").select2();
        $(".table").DataTable();
    });
    function add(){        
        $("#input-bantuan").val("");
        $("#input-keterangan").val("");
        $('#modal-add').modal("show");
        selectedID = "0";
    }
    function doAdd(){        
        $('#modal-add').modal("hide");
        processStart();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>bantuan/add",
            data:{
                id:selectedID,
                bantuan: $("#input-bantuan").val(),
                keterangan : $("#input-keterangan").val()
            }
        }).done(function (msg) {
            processDone();
            $.notify({
                icon: "add_alert",
                message: "Info, <b>"+menuName+" berhasil di simpan</b>"
            }, {
                type: 'success',
                timer: 4000,
                placement: {
                    from: "top",
                    align: "right"
                }
            });
            setTimeout(() => {                
                goMenu("Bantuan","bantuan");
            }, 1000);
        });
    }
    function confirmDelete(id){
        Swal.fire({
            title: 'Hapus data',
            text: "Apakah anda yakin akan meghapus "+menuName+"?",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus'
        }).then((result) => {
            if (result.value) {
                doDelelete(id)
            }
        });
    }
    function getEdit(id){
        selectedID = id;
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>bantuan/detail",
            data: {
                id: id
            },
            statusCode: {
                404: function () {
                    processDone();
                    showError("404 Page not found");
                },
                500: function () {
                    processDone();
                    showError("500 Please contact IT Support");
                }
            },
        }).done(function (msg) {
            var data = eval("("+msg+")");
            $("#input-bantuan").val(data.bantuan);
            $("#input-keterangan").val(data.keterangan);
            $('#modal-add').modal("show");
        });
    }
    function doDelelete(id){
        processStart();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>bantuan/delete",
            data: {
                id: id
            },
            statusCode: {
                404: function () {
                    processDone();
                    showError("404 Page not found");
                },
                500: function () {
                    processDone();
                    showError("500 Please contact IT Support");
                }
            },
        }).done(function (msg) {
            processDone();
            if (msg != "") {
                $.notify({
                    icon: "add_alert",
                    message: "Warning, <b>" + msg + "</b>"
                }, {
                    type: 'danger',
                    timer: 4000,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                });
            } else {
                $.notify({
                    icon: "add_alert",
                    message: "Info, <b>"+menuName+" berhasil di hapus</b>"
                }, {
                    type: 'info',
                    timer: 1000,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                });
                goMenu('Bantuan', 'bantuan');
            }
        });
    }
</script>