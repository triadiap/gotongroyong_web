<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-icon card-header-primary">
                <div class="card-icon">
                    <i class="material-icons">account_circle</i>
                </div>
                <h4 class="card-title">
                    Daftar User
                </h4>
            </div>
            <div class="card-body ">
                <div class="row">   
                    <div class="col-md-12">
                        <input type="button" class="btn btn-primary btn-round" value="Add" onclick="add()" />
                    </div>
                    <div class="col-md-12">
                        <table class="table">
                            <thead>                    
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Pekerjaan</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=0; foreach($data->result() as $tmp) : $i++?>
                                    <tr>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $tmp->username?></td>
                                        <td><?php echo $tmp->nama;?></td>
                                        <td><?php echo $tmp->pekerjaan;?></td>
                                        <td><?php echo $tmp->type;?></td>
                                        <td class="text-center tetx-white">                                    
                                            <a onclick="getEdit('<?php echo $tmp->id?>')" class="btn btn-primary btn-sm btn-round btn-fab btn-fab-mini"><i class="fa fa-pencil-alt text-white"></i><a>
                                            <a onclick="confirmDelete('<?php echo $tmp->id?>')" class="btn btn-danger btn-sm btn-round btn-fab btn-fab-mini"><i class="fa fa-trash text-white"></i><a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-add" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-add-title">Tambah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="#" id="form-add" onsubmit="doAdd();return false;">
                
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input id="input-username" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Password</label>
                        <input id="input-password" type="password" class="form-control">
                    </div>                
                    <div class="form-group">
                        <label for="exampleInputEmail1">Type</label>
                        <select id="input-type" class="form-control select" data-style="btn btn-link"   required>
                            <option value="admin">Admin</option>
                            <option value="user">User</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nama</label>
                        <input id="input-nama" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jenis kelamin</label>
                        <select id="input-jenis_kelamin" class="form-control select" data-style="btn btn-link" required>
                            <option value="L">Laki - Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">NO TLPN.</label>
                        <input id="input-no_tlpn" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">NO KTP</label>
                        <input id="input-no_ktp" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Pekerjaan</label>
                        <input id="input-pekerjaan" type="text" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Lahir</label>
                        <input id="input-tanggal_lahir" type="date" class="form-control" required>
                    </div>        
                </div>
            </div>
            <input type="submit" id="btn-add-submit" style="display:none">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button"  class="btn btn-primary" onclick="$('#btn-add-submit').click()">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
    var menuName = "user";
    var selectedID="0";
    $(function(){
        $(".table").DataTable();
        $('.select select').css('width', '100%');
        $(".select").select2();
        $('.select2').css('width', '100%');
        $('.select2').css('margin-bottom', '10px');
    });
    function add(){        
        $("#input-no_tlpn").val("");
        $("#input-no_ktp").val("");
        $("#input-nama").val("");
        $("#input-jenis_kelamin").val("").trigger('change');;
        $("#input-pekerjaan").val("");
        $("#input-tanggal_lahir").val("");
        $("#input-type").val("").trigger('change');;
        $("#input-username").val("");
        $("#input-password").val("");
        $("#input-password").prop('required',true);

        $('#modal-add').modal("show");
        selectedID="0";
        //$(".select").select2();
    }
    function doAdd(){        
        $('#modal-add').modal("hide");
        processStart();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>user/add",
            data:{
                id:selectedID,
                username: $("#input-username").val(),
                password : $("#input-password").val(),
                type : $("#input-type").val(),
                nama : $("#input-nama").val(),
                jenis_kelamin : $("#input-jenis_kelamin").val(),
                no_tlpn : $("#input-no_tlpn").val(),
                no_ktp : $("#input-no_ktp").val(),
                pekerjaan : $("#input-pekerjaan").val(),
                tanggal_lahir : $("#input-tanggal_lahir").val()
            }
        }).done(function (msg) {
            processDone();
            $.notify({
                icon: "add_alert",
                message: "Info, <b>"+menuName+" berhasil di simpan</b>"
            }, {
                type: 'success',
                timer: 4000,
                placement: {
                    from: "top",
                    align: "right"
                }
            });
            setTimeout(() => {                
                goMenu("User","user");
            }, 1000);
        });
    }
    function confirmDelete(id){
        Swal.fire({
            title: 'Hapus data',
            text: "Apakah anda yakin akan meghapus "+menuName+"?",
            type: 'error',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus'
        }).then((result) => {
            if (result.value) {
                doDelelete(id)
            }
        });
    }
    function getEdit(id){
        selectedID = id;
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>user/detail",
            data: {
                id: id
            },
            statusCode: {
                404: function () {
                    processDone();
                    showError("404 Page not found");
                },
                500: function () {
                    processDone();
                    showError("500 Please contact IT Support");
                }
            },
        }).done(function (msg) {
            var data = eval("("+msg+")");
            $("#input-no_tlpn").val(data.no_tlpn);
            $("#input-no_ktp").val(data.no_ktp);
            $("#input-nama").val(data.nama);
            $("#input-jenis_kelamin").val(data.jenis_kelamin).trigger('change');;
            $("#input-pekerjaan").val(data.pekerjaan);
            $("#input-tanggal_lahir").val(data.tanggal_lahir);
            $("#input-type").val(data.type).trigger('change');;
            $("#input-username").val(data.username);
            $("#input-password").val("");
            $("#input-password").prop('required',false);
            $('#modal-add').modal("show");
            //$(".select").select2();
        });
    }
    function doDelelete(id){
        processStart();
        $.ajax({
            method: "POST",
            url: "<?php echo base_url()?>user/delete",
            data: {
                id: id
            },
            statusCode: {
                404: function () {
                    processDone();
                    showError("404 Page not found");
                },
                500: function () {
                    processDone();
                    showError("500 Please contact IT Support");
                }
            },
        }).done(function (msg) {
            processDone();
            if (msg != "") {
                $.notify({
                    icon: "add_alert",
                    message: "Warning, <b>" + msg + "</b>"
                }, {
                    type: 'danger',
                    timer: 4000,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                });
            } else {
                $.notify({
                    icon: "add_alert",
                    message: "Info, <b>"+menuName+" berhasil di hapus</b>"
                }, {
                    type: 'info',
                    timer: 1000,
                    placement: {
                        from: "top",
                        align: "right"
                    }
                }); 
                goMenu("User","user");
            }
        });
    }
</script>